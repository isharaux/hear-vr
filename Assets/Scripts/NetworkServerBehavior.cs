﻿using UnityEngine;
using UnityEngine.Networking;
using System;
using System.Collections;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using SimpleJSON;

public class NetworkServerBehavior : MonoBehaviour {
	public SpeechCanvasBehavior speechCanvasBehavior;
	private bool mRunning;

	Thread mThread;
	TcpListener listener = null;

	void Awake()
	{
		mRunning = true;
		ThreadStart ts = new ThreadStart(Receive);
		mThread = new Thread(ts);
		Debug.Log ("Starting server thread...");
		mThread.Start();
	}

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () 
	{
	}

	// client function
	public void OnConnected(NetworkMessage netMsg)
	{
		Debug.Log("Connected to server");
	}

	public void stopListening() {
		mRunning = false;
	}

	void Receive() {
		try
		{
			listener = new TcpListener(IPAddress.Any, 7777);
			listener.Start();
			Debug.Log("Server Start");
			TcpClient client = null;
			// Enter the listening loop.
			while(mRunning) 
			{
				Debug.Log("Waiting for a connection... ");

				// Perform a blocking call to accept requests.
				// You could also user server.AcceptSocket() here.
				client = listener.AcceptTcpClient();            
				Debug.Log("Connected!");

				String data = null;

				// Get a stream object for reading and writing
				NetworkStream stream = client.GetStream();
				byte[] buffer = new byte[client.ReceiveBufferSize];

				int i;

				// Loop to receive all the data sent by the client.
				while((i = stream.Read(buffer, 0, buffer.Length))!=0) 
				{   
					// Translate data bytes to a ASCII string.
					data = System.Text.Encoding.ASCII.GetString(buffer, 0, i);
					Debug.Log(String.Format("Received: {0}", data));

					// Unpack the JSON object
					var dict = JSON.Parse(data);

					// Add the speech bubble to canvas
					speechCanvasBehavior.addNewSpeechBubble(dict);

					// Process the data sent by the client.
//					data = data.ToUpper();
//
//					byte[] msg = System.Text.Encoding.ASCII.GetBytes(data);

					// Send back a response.
	//				stream.Write(msg, 0, msg.Length);
	//				Debug.Log(String.Format("Sent: {0}", data));            
				}

				// Shutdown and end connection
				client.Close();
			}
		}
		catch(SocketException e)
		{
			Debug.Log(String.Format("SocketException: {0}", e));
		}
	}

	void OnApplicationQuit() { // stop listening thread
		Debug.Log ("Stopping server thread...");
		stopListening();// wait for listening thread to terminate (max. 500ms)
		mThread.Join(500);
	}
}
