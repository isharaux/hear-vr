﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;

public class SpeechCanvasBehavior : MonoBehaviour {
	public GameObject speechBubblePrefab;
	List<SpeechBubbleBehavior> speechBubbles = new List<SpeechBubbleBehavior>();

	Queue<JSONNode> newSpeechBubbleData = new Queue<JSONNode>();

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		while (newSpeechBubbleData.Count > 0) 
		{
			JSONNode newData = newSpeechBubbleData.Dequeue ();
			var searchResult = speechBubbles.Find (x => x.id == newData ["id"].AsInt);

			GameObject newSpeechBubble = null;
			if (searchResult == null) {
				newSpeechBubble = Instantiate (speechBubblePrefab);
			} else {
				newSpeechBubble = searchResult.gameObject;
			}

			SpeechBubbleBehavior newSpeechBubbleBehavior = newSpeechBubble.GetComponent<SpeechBubbleBehavior> ();
			newSpeechBubble.transform.SetParent (gameObject.transform, false);
			newSpeechBubbleBehavior.id = newData ["id"].AsInt;
			newSpeechBubbleBehavior.setText (newData["cont"]);
			newSpeechBubbleBehavior.setDirection (newData ["dir"]);

			// If this is the final message, remove the bubble after delay
			if (newData ["isFinal"].AsBool == true) {
				StartCoroutine ("removeMessage", newSpeechBubbleBehavior);
			}

			if (searchResult == null) {
				speechBubbles.Add (newSpeechBubbleBehavior);
			}
		}
	}

	IEnumerator removeMessage(SpeechBubbleBehavior speechBubbleBehavior)
	{
		yield return new WaitForSeconds (1.0f);
		
		Destroy (speechBubbleBehavior.gameObject);
	}

	public void addNewSpeechBubble(JSONNode dict)
	{
		newSpeechBubbleData.Enqueue (dict);
	}
}
