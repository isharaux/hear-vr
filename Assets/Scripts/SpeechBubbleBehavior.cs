﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SpeechBubbleBehavior : MonoBehaviour {
	public Text speechText;
	public Image backgroundImage;
	RectTransform backgroundRect;
	public int id;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void setText(string text)
	{
		speechText.text = text;
	}

	public void setDirection(string dir)
	{
		// Load sprite
		string spriteName = "";

		switch (dir) 
		{
		case "FRONTLEFT":
				spriteName = "TopLeft";
				break;
		case "FRONTRIGHT":
				spriteName = "TopRight";
				break;
		case "FRONT":
				spriteName = "Top";
				break;
		case "BACKLEFT":
				spriteName = "BotLeft";
				break;
			case "BACKRIGHT":
				spriteName = "BotRight";
				break;
			case "BACK":
				spriteName = "Bottom";
				break;
		default:
			break;
		}
		Sprite bgSprite = UnityEngine.Resources.Load<Sprite>(spriteName);
		backgroundImage.sprite = bgSprite;

		// Set position
		RectTransform parentRectTransform = transform.parent.GetComponent<RectTransform> ();
		float posBottom = parentRectTransform.rect.height;
		float posRight = parentRectTransform.rect.width;
		float baseX = -posRight * 0.5f;
		float baseY = posBottom * 0.5f;

		RectTransform myRectTransform = GetComponent<RectTransform> ();
		float offX = myRectTransform.rect.width;
		float offY = myRectTransform.rect.height;
		float posX = 0.0f;
		float posY = 0.0f;

		switch (dir) 
		{
		case "FRONTLEFT":
			posX = baseX + 0.0f + offX;
			posY = baseY + 0.0f;
			break;
		case "FRONTRIGHT":
			posX = baseX + posRight;
			posY = baseY + 0.0f;
			break;
		case "FRONT":
			posX = baseX + 0.5f * (posRight + offX);
			posY = baseY + 0.0f;
			break;
		case "BACKLEFT":
			posX = baseX + 0.0f + offX;
			posY = baseY - posBottom + offY;
			break;
		case "BACKRIGHT":
			posX = baseX + posRight;
			posY = baseY - posBottom + offY;
			break;
		case "BACK":
			posX = baseX + 0.5f * (posRight + offX);
			posY = baseY - posBottom + offY;
			break;
		default:
			break;
		}

		Debug.Log ("Setting to " + posX.ToString () + " , " + posY.ToString ());
		gameObject.GetComponent<RectTransform>().localPosition = new Vector3 (posX, posY, 0.0f);
		Debug.Log ("Resulting localPosition: " + gameObject.GetComponent<RectTransform> ().transform.localPosition.ToString ());
	}
}
